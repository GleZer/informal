'use strict';

const { task, series } = require('gulp');
const { src, dest } = require('gulp');
const htmlmin = require('gulp-htmlmin');
const sass = require('gulp-sass');
const watch = require('gulp-watch');
//var pug = require('gulp-pug');
var browserSync = require('browser-sync').create();
var reload = browserSync.reload;

sass.compiler = require('node-sass');

task('build_html', function(cb) {
    build_html();
    cb();
});
task('build_js', function(cb) {
    build_js();
    cb();
});
task('build_php', function(cb) {
    build_php();
    cb();
});
task('build_sass', function(cb) {
    build_sass();
    cb();
});
task('build_css', function(cb) {
    build_css();
    cb();
});

task('build_fa', function(cb) {
    build_fa();
    cb();
});


task('build_gallery', function(cb) {
    build_gallery();
    cb();
});

task('watch', function(cb) {

    browserSync.init({
        open: true,
        server: {
            baseDir: "dist/",
            index: "index.html"
        }
    });

    watch('src/**/*', function() {
        build_all(() => {
            browserSync.reload();
        });
    });

});


// function build_pug() {
//     return src('src/**/*.pug')
//         .pipe(pug({}))
//         .pipe(htmlmin({ collapseWhitespace: true, removeComments: true }))
//         .pipe(dest('dist/'));
// }

function build_html() {
    return src('src/**/*.html')

    .pipe(htmlmin({ collapseWhitespace: true, removeComments: true }))
        .pipe(dest('dist/'));
}

function build_js() {
    return src('src/**/*.js')
        //.pipe(babel({presets: ['@babel/env']}))
        .pipe(dest('dist/'));
}

function build_php() {
    return src('src/**/*.php')
        .pipe(dest('dist/'));
}

function build_sass() {
    return src('src/**/style_*.scss')
        .pipe(sass({ outputStyle: 'compressed' }).on('error', sass.logError))
        .pipe(dest('dist'));
}

function build_css() {
    return src('src/**/style_*.css')
        .pipe(dest('dist'));
}

function build_fa() {
    return src('src/styles/fa/*.scss')
        .pipe(sass({ outputStyle: 'compressed' }).on('error', sass.logError))
        .pipe(dest('dist/styles/fa'));
}

function build_icons() {
    return src('src/images/icons/*.svg')
        .pipe(dest('dist/images/icons'));
}

function build_htaccess() {
    return src('src/**/.htaccess')
        .pipe(dest('dist'));
}

function build_all(cb) {
    //build_pug();
    build_html();
    build_js();
    build_php();
    build_sass();
    build_css();
    build_icons();
    build_htaccess();
    cb();
}

exports.build = build_all;
exports.default = series(build_all);
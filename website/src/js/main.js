"use strict"

NodeList.prototype.forEach = Array.prototype.forEach


var tmp_params = window.location.pathname.split('/').slice(1);
var page_params_tmp = {};
for (let index = 0; index < tmp_params.length; index = index + 2) {
    page_params_tmp[tmp_params[index]] = tmp_params[index + 1];
}

const page_params = page_params_tmp;

class GQL {
    async request(input = {}) {

        var event = Object.keys(input)[0];
        var res_object_name = input[event].split('{')[1].trim();
        res_object_name = res_object_name.split('(')[0].trim();


        return new Promise(function(resolve, reject) {
            console.log(input);
            return fetch("https://informalplace.ru/api", {
                    method: 'POST',
                    cache: 'no-cache',
                    headers: {
                        'Content-Type': 'application/json',
                        'Accept': 'application/json'
                    },
                    body: JSON.stringify(input)
                })
                .then(res => res.json())
                .then(response => {
                    console.log(response.data);
                    resolve(response.data[res_object_name]);
                })
                .catch(error => reject(error));
        });
    }
}

class Cookie {
    get(name) {
        let matches = document.cookie.match(new RegExp(
            "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
        ));
        return matches ? decodeURIComponent(matches[1]) : undefined;
    }

    set(name, value, options = {}) {

        options = {
            path: '/',
            secure: true,
            //domain: '*.informalplace.ru',
            // при необходимости добавьте другие значения по умолчанию
            ...options
        };

        if (options.expires && options.expires.toUTCString) {
            options.expires = options.expires.toUTCString();
        }

        let updatedCookie = encodeURIComponent(name) + "=" + encodeURIComponent(value);

        for (let optionKey in options) {
            updatedCookie += "; " + optionKey;
            let optionValue = options[optionKey];
            if (optionValue !== true) {
                updatedCookie += "=" + optionValue;
            }
        }

        document.cookie = updatedCookie;
    }
}

class TokenPair {

    constructor(access, refresh) {
        this.refresh = refresh;
        this.access = access;
        this.inproc = false;
    }

    payload() {
        var payload = this.access.split('.')[1];
        payload = b64DecodeUnicode(payload);
        return JSON.parse(payload);
    }

    update() {
        var self = this;

        var API = new GQL();
        var Storage = new Cookie();

        self.refresh = Storage.get('Refresh');
        self.access = Storage.get('Access');

        return new Promise(function(resolve, reject) {
            //console.log(this.inproc);







            API.request({
                query: `mutation { updateToken(refresh: "${self.refresh}") {Refresh, Access, log} }`
            }).then((data) => {
                if (data.Refresh && data.Access && data.refresh != "" && data.access != "") {

                    Storage.set('Access', data.Access, {
                        'max-age': 3600 * 24 * 30
                    })

                    Storage.set('Refresh', data.Refresh, {
                        'max-age': 3600 * 24 * 30
                    })

                    self.refresh = data.Refresh;
                    self.access = data.Access;


                    console.log("Токен обновлён");
                    resolve(true);
                } else {
                    alert(data.log);

                    Storage.set('Access', "", {
                        'max-age': 0
                    })

                    Storage.set('Refresh', "", {
                        'max-age': 0
                    })



                    console.log("Токен сброшен");
                    resolve(false);
                }
            })


            //console.log(this.inproc);
        });

    }

    is_exp() {
        var Storage = new Cookie();

        this.refresh = Storage.get('Refresh');
        this.access = Storage.get('Access');

        var now = Math.ceil(Date.now() / 1000);
        return (this.payload().exp < now);
    }

    verify() {
        var API = new GQL();
        var Storage = new Cookie();

        this.refresh = Storage.get('Refresh');
        this.access = Storage.get('Access');

        return API.request({
            query: `{ checkToken(token:"${this.access}") }`
        }).then((data) => {
            return data;

            // 1    - всё ок
            // 0    - истёк
            // -1   - Ложный токен
            // -2   - инвалидировн в базе
            // -3   - Пользователь не сущ
        })
    }
}


function b64DecodeUnicode(str) {
    return decodeURIComponent(atob(str).split('').map(function(c) {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));
}





export { GQL, TokenPair, Cookie, page_params };
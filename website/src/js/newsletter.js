"use strict";

function randomInteger(min, max) {
  let rand = min + Math.random() * (max + 1 - min);
  return Math.floor(rand);
}

// import {resizeGridItem} from '/js/colcade.js';

window.sr = ScrollReveal({
  delay: 100,
  duration: 1000,
  distance: "50px",
  interval: 1000,
  easing: "ease",
  scale: 0.9,
  reset: true,
  mobile: false,
});

var sequenceDelay = 100;
var needload = 0;

class Pub {
  constructor(data) {
    this.pub = document.createElement("div");
    this.pub.className = "card item";
    this.pub.setAttribute("data-scroll-reveal", "left");

    if (!/Android|iPhone|iPad|iPod/i.test(navigator.userAgent)) {
      this.pub.style.opacity = "0";
    }

    var content = document.createElement("div");
    content.className = "content";

    needload++;
    if (data.poster && data.poster != "" && data.poster != "null") {
      var image = document.createElement("img");
      image.className = "main_img";
      image.src = data.poster;

      image.onclick = () => {
        window.open("/article/" + data.ID, "_self");
      };

      image.style.display = "none";
    } else {
      var image = document.createElement("img");
      image.className = "main_img";
      image.src =
        "https://picsum.photos/seed/" +
        randomInteger(0, 500) +
        "/500/" +
        randomInteger(300, 600);

      image.onclick = () => {
        window.open("/article/" + data.ID, "_self");
      };
      image.style.display = "none";
    }

    var title = document.createElement("span");
    title.innerHTML = data.title;
    title.className = "title";

    title.onclick = () => {
      window.open("/article/" + data.ID, "_self");
    };

    var pub_info = document.createElement("div");
    pub_info.className = "pub_info";

    var owner_ph = document.createElement("img");
    owner_ph.className = "owner_ph";
    owner_ph.src = data.author.image;
    pub_info.appendChild(owner_ph);

    var info = document.createElement("div");
    info.className = "info";

    var name = document.createElement("span");
    name.innerHTML = data.author.first_name + " " + data.author.last_name;
    name.className = "name";
    info.appendChild(name);

    var date = document.createElement("span");
    date.innerHTML = "22.02.2019";
    date.className = "date";
    info.appendChild(date);

    pub_info.appendChild(info);
    pub_info.onclick = () => {
      window.open("/user/" + data.author.ID, "_self");
    };

    var social = document.createElement("div");
    social.className = "social";

    var soc_l = document.createElement("div");
    var bookmark = document.createElement("div");
    bookmark.className = "item";
    bookmark.innerHTML = '<img src="/images/icons/bookmark.svg" class="icon">';

    var bookmarks_count = document.createElement("span");
    bookmarks_count.innerHTML = 5;
    bookmark.append(bookmarks_count);

    var comments = document.createElement("div");
    comments.className = "item";
    comments.innerHTML = '<img src="/images/icons/comment.svg" class="icon"/>';

    var comments_count = document.createElement("span");
    comments_count.innerHTML = 8;
    comments.append(comments_count);

    var shares = document.createElement("div");
    shares.className = "item";
    shares.innerHTML = '<img src="/images/icons/share.svg" class="icon"/>';

    var shares_count = document.createElement("span");
    shares_count.innerHTML = 16;
    shares.append(shares_count);

    soc_l.appendChild(bookmark);
    soc_l.appendChild(comments);
    soc_l.appendChild(shares);

    var soc_r = document.createElement("div");

    social.appendChild(soc_l);
    social.appendChild(soc_r);

    content.appendChild(pub_info);
    if (image) {
      content.appendChild(image);
    }

    content.appendChild(title);
    content.appendChild(social);
    this.pub.appendChild(content);
  }

  add(container) {
      var js_container = $(container)[0];
      var pub = this.pub;
      js_container.appendChild(pub);
      imagesLoaded(pub, function(instance, image) {
  
        instance.images.forEach(element => {
          element.img.style.display = "block";
        });
  
        pub.style.opacity = 1;
        MyLib.macyInstance.emit(MyLib.macyInstance.constants.EVENT_IMAGE_LOAD);
  
        needload--;
      });
    }
}

export { Pub };

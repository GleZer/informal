"use strict"

function render(editor, render_block) {
    var render_html = "";
    var nodes = Array.from(editor.children);

    var converter = new showdown.Converter({
        extensions: ['htmlescape'],
        tables: false,
        strikethrough: true,
        simpleLineBreaks: true,
        requireSpaceBeforeHeadingText: true,
        omitExtraWLInCodeBlocks: true,
        smoothLivePreview: true,
        parseImgDimensions: false,
        excludeTrailingPunctuationFromURLs: true,
        literalMidWordUnderscores: true,
        simplifiedAutoLink: true,
        smoothPreview: true,
        backslashEscapesHTMLTags: true
    });
    var html = converter.makeHtml(editor.value);

    render_block.innerHTML = html;
}



function render_card(editor, render, render_card) {
    var h1 = "";
    var poster = "";

    if (render.querySelector('h1') != null) {
        h1 = render.querySelector('h1').innerHTML;
        $(editor).attr('pubtitle', h1);
        //console.log(h1);
    }

    if (render.querySelector('img[alt="Post"]') != null) {
        poster = render.querySelector('img[alt="Post"]').src;
        $(editor).attr('poster', poster);
    }


    //console.log(poster);

    var pubinfo = {
        title: h1,
        poster: poster
    };
    var pub = new Pub(pubinfo);

    if (h1 != "" || poster != "") {
        pub.set('#render_card');
    } else {
        render_card.innerHTML = "";
    }
}

function show_in(btn) {
    if (!$(btn).hasClass('active')) {
        $(render_card_block).fadeOut(200, () => {
            $(render_block).fadeIn(200);
        });
        $("#editor_menu .active").removeClass('active');
        $(btn).addClass('active');
    }
}

function show_out(btn) {
    if (!$(btn).hasClass('active')) {
        $(render_block).fadeOut(200, () => {
            $(render_card_block).fadeIn(200);
        });

        $("#editor_menu .active").removeClass('active');
        $(btn).addClass('active');
    }
}

class Pub {
    constructor(data) {
        this.pub = document.createElement('div');
        this.pub.className = "card item";

        var content = document.createElement('div');
        content.className = 'content';


        if (data.poster) {
            var image = document.createElement('img');
            image.className = 'main_img';
            image.src = data.poster;
        }


        var title = document.createElement('span');
        title.innerHTML = data.title;
        title.className = "title"

        var pub_info = document.createElement('div');
        pub_info.className = "pub_info";

        var owner_ph = document.createElement('div');
        owner_ph.className = "owner_ph";
        pub_info.appendChild(owner_ph);

        var info = document.createElement('div');
        info.className = "info";

        var name = document.createElement('span');
        name.innerHTML = "Антон Медведев";
        name.className = "name";
        info.appendChild(name);

        var date = document.createElement('span');
        date.innerHTML = "22.02.2019";
        date.className = "date";
        info.appendChild(date);

        pub_info.appendChild(info);

        var social = document.createElement('div');
        social.className = "social";

        var soc_l = document.createElement('div');
        var bookmark = document.createElement('div')
        bookmark.className = "item";
        bookmark.innerHTML = '<img src="/images/icons/bookmark.svg" class="icon">';

        var bookmarks_count = document.createElement('span');
        bookmarks_count.innerHTML = 5;
        bookmark.append(bookmarks_count);

        var comments = document.createElement('div');
        comments.className = "item";
        comments.innerHTML = '<img src="/images/icons/comment.svg" class="icon"/>';

        var comments_count = document.createElement('span');
        comments_count.innerHTML = 8;
        comments.append(comments_count);


        var shares = document.createElement('div');
        shares.className = "item";
        shares.innerHTML = '<img src="/images/icons/share.svg" class="icon"/>';

        var shares_count = document.createElement('span');
        shares_count.innerHTML = 16;
        shares.append(shares_count);

        soc_l.appendChild(bookmark);
        soc_l.appendChild(comments);
        soc_l.appendChild(shares);


        var soc_r = document.createElement('div');



        social.appendChild(soc_l);
        social.appendChild(soc_r);

        content.appendChild(pub_info);
        if (data.poster) {
            content.appendChild(image);
        }
        content.appendChild(title);
        content.appendChild(social);
        this.pub.appendChild(content);


    }

    set(container) {
        var js_container = $(container)[0];
        var pub = this.pub;
        js_container.innerHTML = "";
        js_container.appendChild(pub);
    }
}


function editor_htmlEntities(str) {
    //.replace(/\r/g, '')
    //console.log(String(str).replace(/\r/g, 'R').replace(/\n/g, 'N'));
    return String(str).replace(/\r\n\r\n/g, '\n').replace(/\r\n/g, '<br>');
}


export {render, render_card};
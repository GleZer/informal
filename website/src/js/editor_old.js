"use strict"

function render(editor, render_block) {
    var render_html = "";
    var nodes = Array.from(editor.children);

    nodes.forEach(function(item) {
        clear_style(item);
    });


    // console.log(nodes);

    if (nodes.length == 0) {
        var null_obj = document.createElement('p');
        null_obj.innerHTML = "</br>";
        editor.appendChild(null_obj);
    }

    var h1_has = false;


    for (let element_ref of nodes) {
        var inner = element_ref.innerHTML;
        if (inner.match(/&lt;br&gt;/)) {
            inner = inner.replace(/&lt;br&gt;/g, "<br>");
        }
        if (inner.match(/<span>/)) {
            inner = inner.replace(/<span>/g, "").replace(/<\/span>/g, "");
        }



        if (inner.match(/^# (.*)/)) {
            if (!h1_has) {
                var replacement = document.createElement('h1');
                replacement.innerHTML = inner.match(/# (.*)/)[1];
                var render = replacement;

                h1_has = true;
            } else {
                element.innerHTML = "## " + inner.match(/# (.*)/)[1];
            }
        } else
        if (inner.match(/^## (.*)/)) {
            var replacement = document.createElement('h2');
            replacement.innerHTML = inner.match(/^## (.*)/)[1];
            var render = replacement;
        } else
        if (inner.match(/^### (.*)/)) {
            var replacement = document.createElement('h3');
            replacement.innerHTML = inner.match(/^### (.*)/)[1];
            var render = replacement;
        } else
        if (inner.match(/^#### (.*)/)) {
            var replacement = document.createElement('h4');
            replacement.innerHTML = inner.match(/^#### (.*)/)[1];
            var render = replacement;
        } else
        if (inner.match(/^##### (.*)/)) {
            var replacement = document.createElement('h5');
            replacement.innerHTML = inner.match(/^##### (.*)/)[1];
            var render = replacement;
        } else
        if (inner.match(/^###### (.*)/)) {
            var replacement = document.createElement('h6');
            replacement.innerHTML = inner.match(/^###### (.*)/)[1];
            var render = replacement;
        } else
        if (inner.match(/^&gt; (.*)/)) {
            var replacement = document.createElement('blockquote');
            replacement.innerHTML = inner.match(/^&gt; (.*)/)[1];
            var render = replacement;
        } else
        if (inner.match(/^(\s)?&amp;\((.*)\)(\[(.*)\])?(\!\{poster\})?((\s|\n))?$/)) {
            var res = inner.match(/^&amp;\((.*)\)(\[(.*)\])?(\!\{poster\})?((\s|\n))?$/);

            var replacement = document.createElement('figure');

            var img = document.createElement('img');
            img.src = res[1];
            img.alt = "";
            replacement.appendChild(img);


            //console.log(res);
            if (res[3] && res[3].trim()) {
                var caption = document.createElement('figcaption');
                caption.innerHTML = res[3].trim();
                replacement.appendChild(caption);
            }


            var render = replacement;
        } else {
            var render = document.createElement('p');
            render.innerHTML = inner;
        }

        if (render.innerHTML.match(/([\\])?\$\(([_A-Za-z0-9\:\-\.\/]*)\)(\[([A-Za-zА-Яа-я_\:\s0-9]*)\])?/g)) {
            var res = render.innerHTML.match(/([\\])?\$\(([_A-Za-z0-9\:\.\-\/]*)\)(\[([A-Za-zА-Яа-я_\:\s0-9]*)\])?/g);

            //console.log(res);

            for (let href of res) {
                if (href[0] != "\\") {
                    var res = render.innerHTML.match(/\$\(([_A-Za-z0-9\:\.\-\/]*)\)(\[([A-Za-zА-Яа-я_\:\s0-9]*)\])?/);
                    link = document.createElement('a');
                    link.href = res[1].trim();

                    if (res[3] && res[3].trim()) {
                        link.innerHTML = res[3].trim();
                    } else {
                        link.innerHTML = res[1].trim();
                    }


                    render.innerHTML = render.innerHTML.replace(href, link.outerHTML);
                }
            }

        }


        if (render.innerHTML.match(/([\\])?\*(.*?)\*/g)) {
            var res = render.innerHTML.match(/([\\])?\*(.*?)\*/g);


            for (let strike of res) {
                if (strike[0] != "\\") {
                    var replacement = document.createElement('b');
                    replacement.innerHTML = strike.trim().slice(1, -1);

                    const regexp = new RegExp(`\\*${replacement.innerHTML}\\*`, 'g');
                    render.innerHTML = render.innerHTML.replace(regexp, replacement.outerHTML);
                }
            }

        }

        if (render.innerHTML.match(/([\\])?\~(.*?)\~/g)) {
            var res = render.innerHTML.match(/([\\])?\~(.*?)\~/g);


            for (let strike of res) {
                if (strike[0] != "\\") {
                    var replacement = document.createElement('strike');
                    replacement.innerHTML = strike.trim().slice(1, -1);

                    const regexp = new RegExp(`\~${replacement.innerHTML}\~`, 'g');
                    render.innerHTML = render.innerHTML.replace(regexp, replacement.outerHTML);
                }
            }

        }

        if (render.innerHTML.match(/([\\])?\_\_(.*?)\_\_/g)) {
            var res = render.innerHTML.match(/([\\])?\_\_(.*?)\_\_/g);

            //console.log(res);


            for (let strike of res) {
                if (strike[0] != "\\") {
                    var replacement = document.createElement('u');
                    replacement.innerHTML = strike.trim().slice(2, -2);

                    const regexp = new RegExp(`\_\_${replacement.innerHTML}\_\_`, 'g');
                    render.innerHTML = render.innerHTML.replace(regexp, replacement.outerHTML);
                }
            }

        }



        if (render.innerHTML.match(/\!\[center\]/)) {

            render.innerHTML = render.innerHTML.replace(/\!\[center\](((<br>)|\n))?/, "");
            render.style.textAlign = "center";
        }

        if (render.innerHTML.match(/\!\[right\]/)) {
            render.innerHTML = render.innerHTML.replace(/\!\[right\](((<br>)|\n))?/, "");
            render.style.textAlign = "right";
        }

        if (render.innerHTML.match(/\!\[small\]/)) {
            render.innerHTML = render.innerHTML.replace(/\!\[small\](((<br>)|\n))?/, "");
            render.style.fontSize = "10pt";
        }

        if (render.innerHTML.match(/\!\[big\]/)) {
            render.innerHTML = render.innerHTML.replace(/\!\[big\](((<br>)|\n))?/, "");
            render.style.fontSize = "18pt";
        }

        render.innerHTML = render.innerHTML.replace(/\\(.)/g, '$1');

        render_html += render.outerHTML;
    }

    render_block.innerHTML = render_html;

}


document.execCommand("defaultParagraphSeparator", false, "p");

function clear_style(node) {
    node.removeAttribute("style");
    var childrens = Array.from(node.children);
    if (childrens.length > 0) {
        childrens.forEach(function(item) {
            //console.log(item);
            clear_style(item);


        });
    }
}






function render_card(editor, render_card) {
    var render_html = "";
    var nodes = Array.from(editor.children);

    nodes.forEach(function(item) {
        clear_style(item);
    });

    if (nodes.length == 0) {
        var null_obj = document.createElement('p');
        null_obj.innerHTML = "</br>";
        editor.appendChild(null_obj);
    }

    var h1_has = false;
    var h1 = "";

    var poster = "";

    for (let element of nodes) {


        //console.log(element.innerHTML);

        if (element.innerHTML.match(/&lt;br&gt;/)) {
            //console.log('ddd');
            element.innerHTML = element.innerHTML.replace(/&lt;br&gt;/g, "<br>");
        }

        if (element.innerHTML.match(/^# (.*)/)) {
            if (!h1_has) {
                h1_has = true;
                h1 = element.innerHTML.match(/# (.*)/)[1];
            }
        } else
        if (element.innerHTML.match(/^&amp;\((.*)\)(\[(.*)\])?(\!\{poster\})((\s|\n))?$/)) {
            var res = element.innerHTML.match(/^&amp;\((.*)\)(\[(.*)\])?(\!\{poster\})((\s|\n))?$/);

            poster = res[1];
        }

    }


    var pubinfo = {
        title: h1,
        poster: poster
    };
    var pub = new Pub(pubinfo);

    if (h1 != "" || poster != "") {
        pub.set('#render_card');
    } else {
        render_card.innerHTML = "";
    }


    //render_card.innerHTML = h1;
}










class Pub {
    constructor(data) {
        this.pub = document.createElement('div');
        this.pub.className = "card item";

        var content = document.createElement('div');
        content.className = 'content';


        if (data.poster) {
            var image = document.createElement('img');
            image.className = 'main_img';
            image.src = data.poster;
        }


        var title = document.createElement('span');
        title.innerHTML = data.title;
        title.className = "title"

        var pub_info = document.createElement('div');
        pub_info.className = "pub_info";

        var owner_ph = document.createElement('div');
        owner_ph.className = "owner_ph";
        pub_info.appendChild(owner_ph);

        var info = document.createElement('div');
        info.className = "info";

        var name = document.createElement('span');
        name.innerHTML = "Антон Медведев";
        name.className = "name";
        info.appendChild(name);

        var date = document.createElement('span');
        date.innerHTML = "22.02.2019";
        date.className = "date";
        info.appendChild(date);

        pub_info.appendChild(info);

        var social = document.createElement('div');
        social.className = "social";

        var soc_l = document.createElement('div');
        var bookmark = document.createElement('div')
        bookmark.className = "item";
        bookmark.innerHTML = '<img src="./images/icons/bookmark.svg" class="icon">';

        var bookmarks_count = document.createElement('span');
        bookmarks_count.innerHTML = 5;
        bookmark.append(bookmarks_count);

        var comments = document.createElement('div');
        comments.className = "item";
        comments.innerHTML = '<img src="./images/icons/comment.svg" class="icon"/>';

        var comments_count = document.createElement('span');
        comments_count.innerHTML = 8;
        comments.append(comments_count);


        var shares = document.createElement('div');
        shares.className = "item";
        shares.innerHTML = '<img src="./images/icons/share.svg" class="icon"/>';

        var shares_count = document.createElement('span');
        shares_count.innerHTML = 16;
        shares.append(shares_count);

        soc_l.appendChild(bookmark);
        soc_l.appendChild(comments);
        soc_l.appendChild(shares);


        var soc_r = document.createElement('div');



        social.appendChild(soc_l);
        social.appendChild(soc_r);

        content.appendChild(pub_info);
        if (data.poster) {
            content.appendChild(image);
        }
        content.appendChild(title);
        content.appendChild(social);
        this.pub.appendChild(content);


    }

    set(container) {
        var js_container = $(container)[0];
        var pub = this.pub;
        js_container.innerHTML = "";
        js_container.appendChild(pub);
    }
}
"use strict"

function resizeGridItem(item) {
    if (item.querySelector('img')) {
        item.querySelector('img').style.flex = "auto";
    }
    item.querySelector('.content').style.height = 'auto';
    let grid = document.getElementsByClassName("grid")[0];
    let rowHeight = parseInt(window.getComputedStyle(grid).getPropertyValue('grid-auto-rows'));
    let rowGap = parseInt(window.getComputedStyle(grid).getPropertyValue('grid-row-gap'));
    let rowSpan = Math.ceil((item.querySelector('.content').getBoundingClientRect().height + rowGap) / (rowHeight + rowGap));
    item.style.gridRowEnd = "span " + rowSpan;
    item.querySelector('.content').style.height = item.clientHeight + "px";
    if (item.querySelector('img')) {
        item.querySelector('img').style.flex = "1";
    }
}

function resizeAllGridItems() {
    let allItems = document.getElementsByClassName("item");
    for (var x = 0; x < allItems.length; x++) {
        imagesLoaded(allItems[x], resizeInstance);
        resizeGridItem(allItems[x]);
    }
}

function resizeInstance(instance) {
    let item = instance.elements[0];
    resizeGridItem(item);
}

window.addEventListener("resize", resizeAllGridItems);

export {resizeGridItem, resizeAllGridItems, resizeInstance};
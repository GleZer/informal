var app = new Vue({
  el: "#app",
  data: {
    id: 17,
    raw_info: {},
    show: false
  },
  mounted: function() {
    this.show = false;
    axios.post("https://informalplace.ru/api", {
        query: `{
                pub(id: ${this.id}){ID ... on Article{content, title}}
            }`,
      })
      .then(
        function(response) {
          let data = response.data.data.pub;
          data.content = unescape(data.content);
          this.raw_info = data;
          this.show = true;
        }.bind(this),
      );
  },
  computed: {
    content: function() {
      let converter = new showdown.Converter();
      return converter.makeHtml(this.raw_info.content);
    },
    title: function() {
        return this.raw_info.title;
    }
  },
});
